<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 5/18/19
  Time: 11:52 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Formularz studentow</title>
</head>
<body>
<%--
    Napisz formularz dodawania studenta, formularz powinien wyglądać tak, jak poniżej.

    Formularz dodawania studenta:
    Imie:       [           ]
    Nazwisko:   [           ]
    Wiek:       [           ]
    Indeks:     [           ]
                [   Submit  ]
--%>
<form action="dodaj_studenta.jsp">
    <table>
        <tr>
            <td>Imie:</td>
            <td><input type="text" name="imie"></td>
        </tr>
        <tr>
            <td>Nazwisko:</td>
            <td><input type="text" name="nazwisko"></td>
        </tr>
        <tr>
            <td>Wiek:</td>
            <td><input type="number" name="wiek"></td>
        </tr>
        <tr>
            <td>Indeks:</td>
            <td><input type="text" name="indeks"></td>
        </tr>
        <tr>
            <td>Dodaj kolejnego:</td>
            <td><input type="checkbox" name="redirectBack"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit"></td>
        </tr>
    </table>
</form>
</body>
</html>
