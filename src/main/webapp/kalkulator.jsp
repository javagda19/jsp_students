<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 5/18/19
  Time: 9:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Kalkulator</title>
</head>
<body>

<div>
    <form action="kalkulator.jsp"> <%--action= Na jaki adres przekierować--%>
        <input type="number" name="rows" id="rows">
        <input type="number" name="cols" id="cols">
        <input type="submit"> <%-- guzik do przesłania formularza --%>
    </form>
</div>

<%
    String rowsParam = request.getParameter("rows");
    String colsParam = request.getParameter("cols");
    if (rowsParam != null && colsParam != null) {
        int rows = Integer.parseInt(rowsParam);
        int cols = Integer.parseInt(colsParam);
%>
<table border="1">
    <% for (int i = 0; i < rows; i++) {%>
    <tr><%--// wiersz--%>
        <% for (int j = 0; j < cols; j++) {%>
        <td>
            <%out.println((i + 1) * (j + 1));%>
        </td>
        <% }%>
    </tr>
    <% }%>
</table>
<% } %>

</body>
</html>
