<%@ page import="com.javagda19.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 5/18/19
  Time: 1:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dodaj studenta</title>
</head>
<body>
<%
    String imie = request.getParameter("imie");
    String nazwisko = request.getParameter("nazwisko");
    String wiek = request.getParameter("wiek");
    String indeks = request.getParameter("indeks");
    String redirectBack = request.getParameter("redirectBack");

    System.out.println(redirectBack);

    Student student = null;
    if (imie != null && nazwisko != null && wiek != null && indeks != null) {
        student = new Student(imie, nazwisko, Integer.parseInt(wiek), indeks);

        Object objectList = session.getAttribute("studentList"); // załadownie listy z sesji.
        List<Student> studentList;
        if (objectList == null) {
            studentList = new ArrayList<>();    // jeśli w sesji nie ma listy, to ją tworzymy
        } else {
            studentList = (List<Student>) objectList; // jeśli w sesji jest lista, to ją sobie rzutujemy/ładujemy
        }

        studentList.add(student); // dodanie studenta do listy

        session.setAttribute("studentList", studentList); // zapis listy w sesji
    }

    if(redirectBack != null && redirectBack.equalsIgnoreCase("on")) {
        response.sendRedirect(request.getHeader("referer"));
    }else{
        response.sendRedirect("lista_studentow.jsp");
    }
%>
</body>
</html>
