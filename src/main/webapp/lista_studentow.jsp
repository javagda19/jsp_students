<%@ page import="com.javagda19.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 5/18/19
  Time: 11:16 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Lista studentow</title>
</head>
<body>
<%--////////////////////////////////////////////////////////////////////////////////////////////////////////////////--%>
<%
    Object objectList = session.getAttribute("studentList"); // załadownie listy z sesji.
    List<Student> studentList;
    if (objectList == null) {
        studentList = new ArrayList<>();    // jeśli w sesji nie ma listy, to ją tworzymy
    } else {
        studentList = (List<Student>) objectList; // jeśli w sesji jest lista, to ją sobie rzutujemy/ładujemy
    }

    // zadanie 1: stwórz tabelę (poniżej) w której wypiszesz studentów z listy.
    // Lista wypełniona jest wyżej. Wypisz tą listę. Tabela powinna mieć kolumny:
    // Lp., Imie, Nazwisko, Wiek, Numer Indeksu i jedną zapasową.
    // Tabela powinna mieć nagłówek (poszukaj head/ header (thead)) z nazwami kolumn

    //
    // Jeśli w liście nie ma żadnych studentów, wyświetl tabelę (nagłówek) i wiersz
    // z napisem "Brak studentów".
%>
<table border="1">
    <thead>
    <tr>
        <th>Lp.</th>
        <th>Imie</th>
        <th>Nazwisko</th>
        <th>Wiek</th>
        <th>Indeks</th>
        <th></th>
    </tr>
    </thead>
    <% for (int i = 0; i < studentList.size(); i++) { %>
    <tr>
        <td>
            <%out.print(i);%>
        </td>
        <td>
            <%out.print(studentList.get(i).getName());%>
        </td>
        <td>
            <%out.print(studentList.get(i).getSurname());%>
        </td>
        <td>
            <%out.print(studentList.get(i).getAge());%>
        </td>
        <td>
            <%out.print(studentList.get(i).getIndeks());%>
        </td>
        <td>
            <a href="usun_studenta.jsp?studentIndeks=<%=studentList.get(i).getIndeks()%>">Usuń</a>
            <%--// 1. stwórz nowy jsp--%>
            <%--// 2. stwórz blok java, w nim załaduj parametr "studentIndeks"--%>
            <%--// 3. załaduj listę z sesji (listę studentów)--%>
            <%--// 4. na liście znajdź szukanego studenta i go usuń z listy--%>
            <%--// 5. zapisz listę z powrotem do sesji--%>
            <%--// 6. przekieruj użytkownika z powrotem na stronę z listą studentów. --%>
        </td>
    </tr>
    <%}%>
</table>

</body>
</html>
