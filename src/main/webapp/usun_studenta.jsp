<%@ page import="com.javagda19.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 5/18/19
  Time: 1:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Usun studenta</title>
</head>
<body>
<%
    String indeks = request.getParameter("studentIndeks");

    if (indeks != null) {
        Object objectList = session.getAttribute("studentList"); // załadownie listy z sesji.
        List<Student> studentList;
        if (objectList == null) {
            studentList = new ArrayList<>();    // jeśli w sesji nie ma listy, to ją tworzymy
        } else {
            studentList = (List<Student>) objectList; // jeśli w sesji jest lista, to ją sobie rzutujemy/ładujemy
        }

        for (int i = 0; i < studentList.size(); i++) {
            // szukam studenta
            if(studentList.get(i).getIndeks().equalsIgnoreCase(indeks)){
                studentList.remove(i);
                break;
            }
        }

        session.setAttribute("studentList", studentList); // zapis listy w sesji
    }
    response.sendRedirect(request.getHeader("referer"));
%>
</body>
</html>
